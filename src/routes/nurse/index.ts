import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./vital_sign'), { prefix: '/nurse/vital_sign' });
  fastify.register(require('./utility'), { prefix: '/nurse/utility' });
  //fastify.register(require('./assign_bed'), { prefix: '/assign_bed' });
  fastify.register(require('./activity'), { prefix: '/nurse/activity' });
  fastify.register(require('./admit'), { prefix: '/nurse/admit' });

  done();

} 
